# Trap DAC utils

Generic utilities for trap DAC controllers and drivers.

## Usage

Projects may import the utilities found in this repository as packages.

## Utility List

- Parent class for reading from voltage solution and mapping files

## File format

This section describes the file formats used by the trap DAC utils repository.

### Map file

The map file is a csv file that maps pin hardware fields to convenient pin labels. This csv file contains at least two
columns, one contains the labels of the pins, and the remaining columns define hardware specific fields.
Pin labels must be strings that are valid Python identifiers. These identifiers may not begin with an underscore `_`.
Pin labels must be unique and may not appear twice.
The hardware fields provide all information necessary to reference the specific DAC channel routed to the pin.
The map file will contain a header row as its first row, where the column header containing pin labels is `label`
(case-sensitive). The other column headers can be any valid python identifier not beginning with an `_`.
The order of the columns is arbitrary, so long as the column headers are ordered with the correct column values.
A system has one active map file at a time and the map file is used to load solution files.

For example, the single-Zotino trap DAC, only one hardware field is required which must be a unique integer `[31,0]`
which corresponds to the Zotino channel for the pin.
An example of a map file for the single-Zotino implementation:

```
label,channel
PAD_A,3
PAD_B,4
PAD_C,5
```

### Solution file (CSV)

A solution file contains zero or more lines with voltage settings for zero or more pins. The first row (case-sensitive)
must contain a subset of pin labels corresponding to the pin labels in the map file. The remaining rows
(case-insensitive) are called lines and contain voltage values for each column with a pin label on the first row. The
row directly below the pin label (second row) will be considered line 0 and the line numbers increment for each row that
follows. The order of the columns is arbitrary.
Solution files can contain any number of static solutions and/or shuttling solutions. Multiple solution files
can be loaded during experiments.

A voltage value entry of `"x"` or `"X"` (case-insensitive) will cause the pin to not be updated in that voltage solution
line.

Pins that are defined in the map file but not the solution file will be treated as an `"x"`.
An empty value `""` will also be treated as an `"x"`.

An example of a solution file:

```
PAD_A,PAD_B,PAD_C
0,0,0
0,0.5,X
0.5,0.5,1
```

### Config file (JSON/YAML)

A config file, which can be a `.json`, `.yaml`, or `.yml` file, is where specific data objects can be defined for use by
downstream readers. These files can be read in and, along with a schema, can be verified for correctness. Schemas
are provided in the `trap_dac_utils.schemas` package, although custom schemas could also be used.

#### Linear Combo Schema

The linear combo schema allows a user to pass in parameters that consists of single solution lines. The solution
lines are represented by a name, a file, and a line number. Additionally one can define additional args for the line,
useful in creating ARTIQ arguments. The only required field is name.

If file is not provided, the name (along with a `.csv` extension) will be used as a file name. If line number is not
provided, the first line (after the headers) from the csv file will be used.

An example of a linear combo schema `.yaml` file:

```yaml
params:
- name: dx
  file: sequential.csv
  line: 1

- name: dy
  args:
    unit: um
    min: -1
    max: 1
    ndecimals: 3
```

## Testing

This code can be tested by executing the following commands in the root directory of the project.

```shell
$ pytest  # Requires pytest to be installed
$ mypy    # Requires mypy to be installed
$ flake8  # Requires flake8 to be installed
```

