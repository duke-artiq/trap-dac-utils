{ callPackage
, buildPythonPackage
, nix-gitignore
, lib
, natsort
, immutabledict ? callPackage ./immutabledict.nix { }
, schema
, pyyaml
, pytestCheckHook
, numpy
, version ? "0.0.0"
}:

buildPythonPackage {
  pname = "trap-dac-utils";
  inherit version;
  src = nix-gitignore.gitignoreSource [ "*.nix" "/*.lock" ] ./.;

  propagatedBuildInputs = [
    natsort
    immutabledict
    schema
    pyyaml
  ];

  checkInputs = [
    pytestCheckHook
    numpy
  ];

  condaDependencies = [
    "python>=3.7"
    "natsort"
    "immutabledict"
    "schema"
    "pyyaml"
  ];

  meta = with lib; {
    description = "Generic utilities for trap DAC controllers and drivers";
    maintainers = [ "Duke University" ];
    homepage = "https://gitlab.com/duke-artiq/trap-dac-utils";
    license = licenses.mit;
  };
}
