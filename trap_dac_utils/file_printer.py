import csv
import pathlib
from functools import partial
import typing

from trap_dac_utils.types import SOLUTION_FILE_T

__all__ = ['print_solution']


def print_solution(file_path: pathlib.Path,
                   solution: SOLUTION_FILE_T,
                   *,
                   overwrite: bool = False) -> None:
    """Prints a python object into a file

    :param file_path: Path of file to print
    :param solution: The solution object to print to a file
    :param overwrite: Overwrite file with new solution when true
        Otherwise raise FileExistsError
    """

    assert isinstance(file_path, pathlib.Path)

    # Create partial functions for different file types
    fn_dict: typing.Dict[str, partial[None]]
    fn_dict = {'.csv': partial(_print_csv, file_path, solution)}

    # Can't overwrite existing file
    if not overwrite and file_path.is_file():
        raise FileExistsError(f'File {file_path.name} already exists')

    try:
        # Call the corresponding partial function
        fn_dict[file_path.suffix]()
    except KeyError:
        raise ValueError(f"File type should be in list "
                         f"{list(fn_dict.keys())}") from None


def _print_csv(file_path: pathlib.Path,
               solution: SOLUTION_FILE_T) -> None:
    """Prints a python object into a csv file

    :param file_path: Path of csv file to print
    :param solution: The solution object to print to the file
    """
    with open(file_path, 'w') as file:
        if len(solution) > 0:
            # Print raw values from CSV
            writer = csv.DictWriter(file, fieldnames=list(solution[0].keys()))
            writer.writeheader()
            writer.writerows(solution)
