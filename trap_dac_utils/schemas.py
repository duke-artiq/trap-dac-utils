from schema import Optional, Schema   # type: ignore[import]

__all__ = ['LINEAR_COMBO', 'SMART_DMA']

LINEAR_COMBO = Schema({
    Optional("params"): [{
        "name": str,
        Optional("file"): str,
        Optional("line"): int,
        Optional("args"): {
            Optional("unit"): str,
            Optional("min"): float,
            Optional("max"): float,
            Optional("ndecimals"): int,
            Optional("scale"): float,
            Optional("step"): float
        }
    }]
})

SMART_DMA = Schema({
    Optional("params"): [{
        "name": str,
        Optional("file"): str,
        Optional("start"): int,
        Optional("end"): int,
        "line_delay": float,
        Optional("reverse"): bool,
        Optional("multiplier"): float
    }]
})
