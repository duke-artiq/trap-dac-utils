# mypy: no_warn_unused_ignores

import collections.abc
import csv
from functools import lru_cache, partial
import typing
import pathlib
import importlib
from immutabledict import immutabledict
import json
import yaml  # type: ignore[import]

from trap_dac_utils.types import SOLUTION_FILE_T, CONFIG_FILE_T

__all__ = ['load_config', 'load_solution']


@lru_cache(maxsize=50)
def _load_csv(file_path: pathlib.Path) -> SOLUTION_FILE_T:
    """Loads a csv file into a python object

    :param file_path: Path of csv file to load

    :return: List of dictionaries representing each row in the csv file
    """
    with open(file_path) as file:
        # Read headers
        keys = file.readline().strip().split(",")

        # Assert uniqueness of headers
        if len(set(keys)) != len(keys):
            raise ValueError(f'All headers in {file_path.name} '
                             'must be unique')

    with open(file_path) as file:
        # Read raw values from CSV
        reader = csv.DictReader(file)
        raw: SOLUTION_FILE_T = tuple(immutabledict(d) for d in reader)

    return raw


@lru_cache(maxsize=50)
def _load_py(file_path: pathlib.Path,
             map_file: SOLUTION_FILE_T) -> SOLUTION_FILE_T:
    """Loads a python file into a python object

    :param file_path: Path of python file to load
    :param map_file: Loaded interpretation of map file

    :return: List of dictionaries representing each row in the python
    file
    """
    # Make the map file immutible
    map_file = tuple(immutabledict(d) for d in map_file)
    # Get the python module that generates a solution
    m = importlib.import_module('.'.join(file_path.parts)[:-3])

    raw: SOLUTION_FILE_T
    try:
        # Make the returned solution immutible
        raw = tuple(immutabledict(d)
                    for d
                    in m.solution(map_file))  # type: ignore[attr-defined]
    # Check for module/function correctness
    except AttributeError:
        raise AttributeError('A solution() method must be in your file')
    except TypeError:
        raise TypeError('Argument signature should be '
                        '(Sequence[Dict[str, str]])') from None
    if not _check_py_dicts_t(raw):
        raise TypeError('Return signature should be '
                        '(Sequence[Dict[str,str|float]])')
    return raw


@lru_cache(maxsize=50)
def _load_json(file_path: pathlib.Path) -> CONFIG_FILE_T:
    """Loads a json file into a python object

    :param file_path: Path of json file to load

    :return: A dictionary of str to obj representing the json
    file structure
    """
    with open(file_path) as file:
        raw: CONFIG_FILE_T = json.load(file)

    return raw


@lru_cache(maxsize=50)
def _load_yaml(file_path: pathlib.Path) -> CONFIG_FILE_T:
    """Loads a yaml file into a python object

    :param file_path: Path of yaml file to load

    :return: A dictionary of str to obj representing the yaml
    file structure
    """
    with open(file_path) as file:
        raw: CONFIG_FILE_T = yaml.load(file, Loader=yaml.FullLoader)

    return raw


def _check_py_dicts_t(obj: typing.Any) -> bool:
    """Checks if an object if of type _PY_DICTS_T
    PY_DICTS_T is a sequence of dictionaries that has keys of type
    str and values of type float or str

    :param obj: Any object to type check

    :return: True if it is of type _PY_DICTS_T, otherwise False
    """
    # type check for outer container
    if not isinstance(obj, collections.abc.Sequence):
        return False
    # type check for inner container
    for d in obj:
        if not isinstance(d, collections.abc.Mapping):
            return False
        if not all(isinstance(k, str) for k in d.keys()):
            return False
        if not all(isinstance(v, (str, float))
                   for v in d.values()):
            return False
    return True


def load_solution(file_path: pathlib.Path,
                  map_file: SOLUTION_FILE_T) -> SOLUTION_FILE_T:
    """Loads a solution file into a python object

    :param file_path: Path of file to load
    :param map_file: Map file to pass to python file loader

    :return: List of dictionaries representing each row in the file
    """
    assert isinstance(file_path, pathlib.Path)

    # Create partial functions for different file types
    fn_dict: typing.Dict[str, partial[SOLUTION_FILE_T]]
    fn_dict = {'.csv': partial(_load_csv, file_path),
               '.py': partial(_load_py, file_path, map_file)}
    try:
        # Call the corresponding partial function
        raw = fn_dict[file_path.suffix]()
    except KeyError:
        raise ValueError(f"File type should be in list "
                         f"{list(fn_dict.keys())}") from None

    return raw


def load_config(file_path: pathlib.Path) -> CONFIG_FILE_T:
    """Loads a config file into a python object

    :param file_path: Path of file to load

    :return: A single dictionary of strings to objects representing the
    config
    """
    assert isinstance(file_path, pathlib.Path)

    # Create partial functions for different file types
    fn_dict: typing.Dict[str, partial[CONFIG_FILE_T]]
    fn_dict = {'.json': partial(_load_json, file_path),
               '.yml': partial(_load_yaml, file_path),
               '.yaml': partial(_load_yaml, file_path)}
    try:
        # Call the corresponding partial function
        raw = fn_dict[file_path.suffix]()
    except KeyError:
        raise ValueError(f"File type should be in list "
                         f"{list(fn_dict.keys())}") from None

    return raw
