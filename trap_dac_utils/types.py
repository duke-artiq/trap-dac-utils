from __future__ import annotations
import hashlib
import math
import numbers
import struct
import enum
import typing
from immutabledict import immutabledict

__all__ = ['SpecialCharacter', 'SOLUTION_KEY_T', 'SOLUTION_VALUE_T', 'LINE_T',
           'SOLUTION_T', 'MAP_KEY_T', 'MAP_VALUE_T', 'MAP_T', 'LABEL_FIELD',
           'CONFIG_T', 'SOLUTION_FILE_T', 'CONFIG_FILE_T']


@enum.unique
class SpecialCharacter(enum.Enum):
    """Enum for possible Special Characters in solutions file

    Add more Special Character definitions here as new use cases
    arise
    """
    X = 'x'

    def __add__(self, other: typing.Any) -> SOLUTION_VALUE_T:
        if isinstance(other, SpecialCharacter):
            # Check for same special character value
            if self is other:
                return self
            else:
                raise ValueError(f"unsupported operand values for +: {self.value} and {other.value}")
        elif isinstance(other, numbers.Number):
            # X added to number does not give back a number
            if self == SpecialCharacter.X:
                return float('nan')
            else:
                raise ValueError(f"unsupported operand values for +: {self.value} and any number")
        else:
            raise TypeError(f"unsupported operand type(s) for +: '{SpecialCharacter}' and '{type(other)}'")

    __radd__ = __add__

    def __mul__(self, other: typing.Any) -> SOLUTION_VALUE_T:
        if isinstance(other, SpecialCharacter):
            # Check for same special character value
            if self is other:
                return self
            else:
                raise ValueError(f"unsupported operand values for *: {self.value} and {other.value}")
        # X multiplied by number does not give back a number
        elif isinstance(other, numbers.Number):
            if self == SpecialCharacter.X:
                return float('nan')
            else:
                raise ValueError(f"unsupported operand values for *: {self.value} and any number")
        else:
            raise TypeError(f"unsupported operand type(s) for *: '{SpecialCharacter}' and '{type(other)}'")

    __rmul__ = __mul__

    @classmethod
    def is_valid(cls, val: str) -> bool:
        """Check if a value is a possible special character

        :param val: String value from solutions file

        :return: True if it is a Special Character
        """
        if not isinstance(val, str):
            return False

        try:
            SpecialCharacter(val)
        except ValueError:
            return False
        return True


# Map and Solution types
SOLUTION_KEY_T = str
"""Solution file representation key type"""
SOLUTION_VALUE_T = typing.Union[float, SpecialCharacter]
"""Solution file representation value type"""


class Line(immutabledict[SOLUTION_KEY_T, SOLUTION_VALUE_T]):
    """Generic python representation of a solution file row"""

    def __add__(self, other: typing.Any) -> Line:
        if isinstance(other, Line):
            if self.keys() != other.keys():
                raise ValueError('Objects to add must have same key structure')
            # Create new line where each value is the addition of values with same key
            line = Line({k: self[k] + other[k] for k in self.keys()})
            # Check if the above produces any nan values
            if any(math.isnan(val) for _, val in line.items() if isinstance(val, float)):
                raise ValueError(f'operation + resulted in a banned float value '
                                 f'self {self} '
                                 f'other {other} '
                                 f'result {line}')
            return line
        else:
            raise TypeError(f"unsupported operand type(s) for +: '{Line}' and '{type(other)}'")

    __radd__ = __add__

    def __mul__(self, other: typing.Any) -> Line:
        if isinstance(other, Line):
            if self.keys() != other.keys():
                raise ValueError('Objects to add must have same key structure')
            # Create new line where each value is the multiplication of values with same key
            line = Line({k: self[k] * other[k] for k in self.keys()})
            # Check if the above produces any nan values
            if any(math.isnan(val) for _, val in line.items() if isinstance(val, float)):
                raise ValueError(f'operation * resulted in a banned float value '
                                 f'self {self} '
                                 f'other {other} '
                                 f'result {line}')
            return line
        else:
            raise TypeError(f"unsupported operand type(s) for *: '{Line}' and '{type(other)}'")

    __rmul__ = __mul__


LINE_T = Line
"""Solution file row representation type"""


class HashedTuple(typing.Tuple[LINE_T, ...]):
    """Generic python representation of a CSV solution file

    This class provides a hash for the solution object for efficient comparisons"""

    _hash: str

    def __new__(cls, *args: typing.Any, **kwargs: typing.Any) -> HashedTuple:
        self = super(HashedTuple, cls).__new__(cls, *args, **kwargs)
        self._hash = ''
        return self

    def __add__(self, other: typing.Any) -> HashedTuple:
        if isinstance(other, HashedTuple):
            if len(self) != len(other):
                raise ValueError('Objects to add must have same length')
            rows: typing.List[LINE_T] = []
            for i in range(len(self)):
                rows.append(self[i] + other[i])
            return HashedTuple(tuple(rows))
        else:
            raise TypeError(f"unsupported operand type(s) for +: '{HashedTuple}' and '{type(other)}'")

    __radd__ = __add__

    def __mul__(self, other: typing.Any) -> HashedTuple:
        if isinstance(other, HashedTuple):
            if len(self) != len(other):
                raise ValueError('Objects to multiply must have same length')
            rows: typing.List[LINE_T] = []
            for i in range(len(self)):
                rows.append(self[i] * other[i])
            return HashedTuple(tuple(rows))
        else:
            raise TypeError(f"unsupported operand type(s) for *: '{HashedTuple}' and '{type(other)}'")

    __rmul__ = __mul__

    @property
    def hash(self) -> str:
        """Will return a hash based on the solution

        :return: Hexadecimal representation of the hash of the solution
        """
        if not self._hash:
            solution_hash = hashlib.sha256()
            for d in self:
                for k, v in d.items():
                    solution_hash.update(k.encode())
                    if isinstance(v, float):
                        solution_hash.update(struct.pack('f', v))
                    else:
                        solution_hash.update(str(v).encode())
            self._hash = solution_hash.hexdigest()

        return self._hash


SOLUTION_T = HashedTuple
"""The high-level solution type for a read-in solution file."""

MAP_KEY_T = str
"""Map file representation key type"""
MAP_VALUE_T = str
"""Map file representation value type"""
MAP_T = typing.Sequence[typing.Mapping[MAP_KEY_T, MAP_VALUE_T]]
"""Map file python object representation"""

CONFIG_T = typing.Mapping[str, typing.Any]
"""Config file python object representation"""

# File python types
SOLUTION_FILE_T = typing.Sequence[typing.Mapping[str, typing.Union[str,
                                                                   float]]]
"""The python object representation of loaded solution files"""
CONFIG_FILE_T = typing.Mapping[str, typing.Any]
"""The python object representation of loaded config files"""


LABEL_FIELD: str = 'label'
"""Map file mandatory header field"""
