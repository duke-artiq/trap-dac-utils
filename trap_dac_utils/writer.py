import typing
import pathlib

from trap_dac_utils.types import SOLUTION_T, SOLUTION_VALUE_T, HashedTuple, SpecialCharacter
from trap_dac_utils.types import LABEL_FIELD, MAP_T
from trap_dac_utils.file_printer import print_solution

__all__ = ['BaseWriter']


class BaseWriter:

    def __init__(self,
                 solution_path: pathlib.Path,
                 map_file: MAP_T,
                 allowed_specials: typing.FrozenSet[str]
                 = frozenset(SpecialCharacter),
                 precision: int = 7):
        """Constructor of a base writer class.

        :param solution_path: Path to the directory containing solution
        files
        :param map_file: Path to the map file used to map pins to
        hardware output channels
        :param allowed_specials: A set of string characters that are
        allowed in the solution files (not including numbers)
        """

        assert isinstance(solution_path, pathlib.Path)

        # Get the solution path
        self._solution_path = solution_path
        if not self._solution_path.is_dir():
            raise NotADirectoryError(
                f'{self._solution_path} is not a directory')

        self._map_file = map_file

        self._allowed_specials = allowed_specials

        self._precision = precision

    @property
    def solution_path(self) -> str:
        """Get the solution path

        :return: The path to the solution file directory
        """
        return str(self._solution_path)

    @property
    def map_file(self) -> MAP_T:
        """Get the converted python representation of the map file

        :return: The map file object
        """
        return self._map_file

    @property
    def label(self) -> str:
        """Get the label column header for map files

        :return: The label column header for map files
        """
        return LABEL_FIELD

    def _process_solution_value(self, value: SOLUTION_VALUE_T) -> str:
        """Convert solution value to a string

        :param value: Solution value

        :return: The string representation of the solution value
        """

        if isinstance(value, float):
            return f'{value:.{self._precision}f}'
        elif isinstance(value, SpecialCharacter) and value.value in self._allowed_specials:
            return str(value.value)
        else:
            raise ValueError("Value must be either a float"
                             " or a valid SpecialCharacter")

    def write_solution_np(self,
                          solution_np: typing.Iterable[typing.Iterable[float]],
                          labels: typing.Sequence[str],
                          file_name: str,
                          *,
                          overwrite: bool = False) -> None:
        """Write a numpy solution representation of a solutions file
        to the specified file

        :param solution_np: The rectangular numpy list of solution lines
        :param labels: The list of labels to use as header for the solution lines
            Should have the same length as number of columns in solution_np
        :param file_name: The file name to write the solution file to
        :param overwrite: A boolean to signal whether to overwrite the given file
        """
        solution = HashedTuple(({k: v for k, v in zip(labels, line)} for line in solution_np))

        self.write_solution(solution, file_name, overwrite=overwrite)

    def write_solution(self,
                       solution: SOLUTION_T,
                       file_name: str,
                       *,
                       overwrite: bool = False) -> None:
        """Write a CSV solution representation of a solutions file
        to the specified file

        :param solution: The generic python representation of the solution file
        :param file_name: The file name to write the solution file to
        :param overwrite: A boolean to signal whether to overwrite the given file
        """
        labels = self._list_map_labels()
        if len(solution) != 0 and not all(header in labels
                                          for header in solution[0]):
            raise ValueError('Headers in solution must be a '
                             'map file label')
        # Process each value in the solution before printing
        processed_solution = [{k: self._process_solution_value(v)
                              for k, v in dict_line.items()}
                              for dict_line in solution]
        print_solution(self._solution_path.joinpath(file_name),
                       processed_solution,
                       overwrite=overwrite)

    def _list_map_labels(self) -> typing.Sequence[str]:
        """Lists the labels that are in the map file

        :return: The labels from the map file
        """
        return [d[LABEL_FIELD] for d in self.map_file]
