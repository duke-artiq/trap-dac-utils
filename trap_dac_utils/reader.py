# mypy: no_warn_unused_ignores

import math
from immutabledict import immutabledict
import typing
import abc
import natsort
import pathlib
from schema import Schema  # type: ignore[import]
from trap_dac_utils.types import SpecialCharacter, SOLUTION_T, CONFIG_T, \
    SOLUTION_VALUE_T, MAP_T, LABEL_FIELD
from trap_dac_utils.file_loader import load_solution, load_config

__all__ = ['BaseReader']


def is_float(val: str) -> bool:
    """Simple function to determine if string value is a float

    :param val: Value to check if float

    :return: True if can be converted to float, otherwise false
    """
    try:
        float(val)
    except ValueError:
        return False
    return True


_T = typing.TypeVar('_T')


class BaseReader(abc.ABC, typing.Generic[_T]):
    """A reader parent to read in solutions and be overwritten by
    hardware specific child readers

    This class reads in a map file and provides methods to read
    in solutions in a general form that need to be extended for
    specific hardware devices

    Notes when considering using this reader:

    - There are both abstract methods and properties that need to
    be implemented by the child classes
    """

    _LABEL: typing.ClassVar[str] = LABEL_FIELD
    """Column key for labels.
    Left here for backwards compatability, should instead use LABEL_FIELD directly"""

    def __init__(self,
                 solution_path: pathlib.Path,
                 map_file: pathlib.Path,
                 allowed_specials: typing.FrozenSet[str]
                 = frozenset(SpecialCharacter)):
        """Constructor of a base reader class.

        :param solution_path: Path to the directory containing solution
        files
        :param map_file: Path to the map file used to map pins to
        hardware output channels
        :param allowed_specials: A set of string characters that are
        allowed in the solution files (not including numbers)
        """

        assert isinstance(solution_path, pathlib.Path)
        assert isinstance(map_file, pathlib.Path)

        # Get the solution path
        self._solution_path = solution_path
        if not self._solution_path.is_dir():
            raise NotADirectoryError(
                f'{self._solution_path} is not a directory')

        self._map_file = self._read_channel_map(map_file)

        self._allowed_specials = allowed_specials

    @property
    @abc.abstractmethod
    def voltage_high(self) -> float:
        """Override to define the highest allowable voltage in the
        solutions file

        :return: The highest allowed voltage (inclusive)
        """
        pass

    @property
    @abc.abstractmethod
    def voltage_low(self) -> float:
        """Override to define the lowest allowable voltage in the
        solutions file

        :return: The lowest allowed voltage (inclusive)
        """
        pass

    @property
    def map_file(self) -> MAP_T:
        """Get the converted python representation of the map file

        :return: The map file object
        """
        return self._map_file

    @property
    def solution_path(self) -> str:
        """Get the solution path

        :return: The path to the solution file directory
        """
        return str(self._solution_path)

    @property
    def label(self) -> str:
        """Get the label column header for map files

        :return: The label column header for map files
        """
        return LABEL_FIELD

    def _read_channel_map(self, map_file: pathlib.Path) -> MAP_T:
        """Read valid map file csv into a python interpretable object
        Map File must contain one column for labels labeled 'label'
        and at least one column for categories that identify a hardware
        channel.
        Each row must correspond to a unique hardware channel

        :param map_file: Path to the map file used to map pin labels to
        hardware output channels

        :return: A list of dictionaries for each row in the map file
        with the header of each column as the keys
        """
        # cast to a _MAP_T type since map will be CSV file
        dict_channel_map = typing.cast(MAP_T, load_solution(
            map_file, ()))
        if not map_file:
            raise FileNotFoundError(f'{map_file} file not found')
        if len(dict_channel_map) == 0:
            raise ValueError(
                f'Channel map {map_file.name} does not contain any data rows')
        if len(dict_channel_map[0]) < 2:
            raise ValueError(
                f'Channel map {map_file.name} does not contain enough columns')
        if LABEL_FIELD not in dict_channel_map[0].keys():
            raise ValueError(
                f'Channel map {map_file.name} must contain a label column')
        if not all(header.isidentifier()
                   for header in dict_channel_map[0].keys()):
            raise ValueError(
                f'Channel map {map_file.name} headers must '
                f'be python identifiers')
        if any(header.startswith('_')
               for header in dict_channel_map[0].keys()):
            raise ValueError(
                f'Channel map {map_file.name} headers '
                f'must not start with _')

        self.__check_label_values(dict_channel_map, map_file.name)

        return tuple(immutabledict(d) for d in dict_channel_map)

    def __check_label_values(self,
                             dict_channel_map: MAP_T,
                             map_file_name: str) -> None:
        """Checks the label values to ensure uniqueness, valid python
        identifiers, and doesn't start with _

        :param dict_channel_map: Map file fith labels to verify
        :param map_file_name: Name of map file
        """
        used_labels = set()
        for d in dict_channel_map:
            if not d[LABEL_FIELD].isidentifier():
                raise ValueError(
                    f'Channel map {map_file_name} labels must be python '
                    f'identifiers')
            if d[LABEL_FIELD].startswith('_'):
                raise ValueError(
                    f'Channel map {map_file_name} labels must not start '
                    f'with _')
            if d[LABEL_FIELD] in used_labels:
                raise ValueError(
                    f'Channel map {map_file_name} labels must be unique')
            used_labels.add(d[LABEL_FIELD])

    def list_solutions(self) -> typing.Sequence[str]:
        """Get a list of each solution file available in the solutions
        directory

        :return: The list of names of solution files available
        """
        # Find all .csv and .py files
        file_exts = ('*.csv', '*.py')
        all_files: typing.List[pathlib.Path] = []
        for ext in file_exts:
            all_files.extend(self._solution_path.rglob(ext))
        # Get the relative path (to solutions directory) of all solution files
        profiles = typing.cast(typing.List[str], natsort.natsorted(
            [x for x in [str(f.relative_to(self._solution_path))
                         for f in all_files]
             if not x.endswith('__init__.py')]
        ))
        return profiles

    def _process_solution_value(self,
                                value: typing.Union[float,
                                                    str]) -> SOLUTION_VALUE_T:
        """Determine if a solution value is a number, special character,
        or unallowed

        :param value: Solution value

        :return: The non-string representation of the solution value
        """
        if isinstance(value, float):
            if not math.isnan(value):
                return value
            else:
                raise ValueError('Value cannot be nan')
        elif isinstance(value, str):
            value = value.strip().lower()

            # empty cells are treated like "X" values
            if value == '':
                value = 'x'

            if is_float(value):
                f = float(value)
                if math.isnan(f):
                    raise ValueError('Value cannot be nan')
                # infs convert to max and min values
                if f == float('inf'):
                    f = self.voltage_high
                elif f == float('-inf'):
                    f = self.voltage_low
                if not self._value_in_range(f):
                    raise ValueError(
                        f'Voltage {value} must be in range '
                        f'{self.voltage_low} to {self.voltage_high}')
                return f
            elif SpecialCharacter.is_valid(value):
                if SpecialCharacter(value) in self._allowed_specials:
                    return SpecialCharacter(value)
                else:
                    raise ValueError(
                        f'Special character {value} is disabled')
            else:
                raise ValueError(
                    f'Character {value} is not float or special character')

        else:
            raise TypeError('Value to process must be of type str or float')

    def _value_in_range(self, f: float) -> bool:
        """Check that a float is in the value range of this reader instance

        :param f: Float value to check

        :return: True if the value is in the range inclusive, otherwise false
        """
        return f <= self.voltage_high and f >= self.voltage_low

    def verify_solution(self, solution: SOLUTION_T) -> bool:
        """Given a solution check that it is valid for this reader instance

        For example, must check that all floats are in the accepted range

        :param solution: Solution to verify against reader instance

        :return: True if the solution is allowed for this instance, otherwise false
        """
        in_range = all(self._value_in_range(v)
                       for row in solution
                       for _, v in row.items()
                       if isinstance(v, float))
        return in_range

    def read_solution(self,
                      file_name: str,
                      start: int = 0,
                      end: int = -1) -> SOLUTION_T:
        """Read valid solution file into a python interpretable object
        First line of solution are unique pin label identifiers
        The other lines are values which must be either Floats or Special
        Characters
        Results are cached with a max size of 50

        :param file_name: Name of the solutions file to read
        :param start: Starting index of solution (inclusive). Default 0
            signals to start with first solution line
        :param end: End index of solution (inclusive). Default -1 signals
            to end with last solution line

        :return: Python object representation of the solutions file
        """
        dict_solution = load_solution(
            self._solution_path.joinpath(file_name), self.map_file)
        if not all(header in self.list_map_labels()
                   for header in dict_solution[0]):
            invalid_labels = [header for header in dict_solution[0] if header not in self.list_map_labels()]
            raise ValueError(f'Headers in {file_name} must be a '
                             f'map file label '
                             f'invalid labels: {invalid_labels}')

        if end == -1:
            end = len(dict_solution) - 1

        # If label undefined value is 'x', otherwise process original value
        return SOLUTION_T(
            immutabledict({label: self._process_solution_value(dict_line[label])
                           if label in dict_line else SpecialCharacter('x')
                           for label in self.list_map_labels()})
            for dict_line in dict_solution[start:end+1])

    def read_config(self,
                    file_name: str,
                    *,
                    schema: typing.Optional[Schema] = None) -> CONFIG_T:
        """Function for reading the configuration file upon initialiation
        of the reader

        :param file_name: The path for the configuration directory
        :param schema: The schema to validate the configuration object
            against

        :return: A python representation of the configuration structure
        """
        file_dict = load_config(self._solution_path.joinpath(file_name))

        if schema is not None:
            schema.validate(file_dict)

        return file_dict

    def num_labels(self) -> int:
        """Returns the number of labels in the map file

        :return: The number of labels in the map file
        """
        return len(self.map_file)

    def list_map_labels(self) -> typing.Sequence[str]:
        """Lists the labels that are in the map file

        :return: The labels from the map file
        """
        return [d[LABEL_FIELD] for d in self.map_file]

    def read_all_solutions(self,
                           glob: str) -> typing.Dict[str, SOLUTION_T]:
        """Read in all solution files based on a given glob definition
        Will be done recursively using rglob and can access with the file name

        :param glob: Glob string to recursively find files in solution
        directory

        :return: A Dictionary of file names to python object representations
        of the file
        """
        p = pathlib.Path(self._solution_path)
        all_files = list(p.glob(glob))
        # generate solution python objects for each matched solution path
        return {str(file.relative_to(self._solution_path)): self.read_solution(
            str(file.relative_to(self._solution_path)))
            for file in all_files}

    @abc.abstractmethod
    def process_specials(self, val: SpecialCharacter) -> typing.Any:
        """Override this method to process the allowed special character
        Should be called in the :func:`parse_solution` implementation

        :param val: Enum SpecialCharacter value

        :return: Type that will convert SpecialCharacter, type may vary
        with implementation
        """
        pass

    @abc.abstractmethod
    def process_solution(self,
                         solution: SOLUTION_T) -> _T:
        """Override this method to process the generic solution
        file interpretation to a hardware specific form
        The input to this

        :param solution: The solution file returned by a call to
        :func:`read_solution`

        :return: The hardware specific interpretation of a solution file
        """
        pass
