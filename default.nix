{ pkgs ? import <nixpkgs> { }
, immutabledict ? (if builtins.hasAttr "immutabledict" pkgs.python3Packages then pkgs.python3Packages.immutabledict else pkgs.python3Packages.callPackage ./immutabledict.nix { })
}:

pkgs.python3Packages.callPackage ./derivation.nix { inherit immutabledict; }
