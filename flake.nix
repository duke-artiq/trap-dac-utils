{
  inputs = {
    nixpkgs.url = github:NixOS/nixpkgs/nixos-22.05;
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
        trap-dac-utils = pkgs.python3Packages.callPackage ./derivation.nix {
          version = "${builtins.toString self.sourceInfo.lastModifiedDate or 0}+${self.sourceInfo.shortRev or "unknown"}";
        };
      in
      {
        packages = {
          inherit trap-dac-utils;
          default = trap-dac-utils;
        };

        devShells = {
          default = pkgs.mkShell {
            packages = [
              (pkgs.python3.withPackages (ps: [
                # Packages required for testing
                ps.autopep8
                ps.pytest
                ps.coverage
                ps.mypy
                ps.flake8
                ps.numpy
              ] ++ trap-dac-utils.propagatedBuildInputs))
            ];
          };
        };

        # enables use of `nix fmt`
        formatter = pkgs.nixpkgs-fmt;
      }
    );
}
