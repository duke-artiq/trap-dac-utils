{ pkgs ? import <nixpkgs> { }
}:

let
  trap-dac-utils = import ./default.nix { inherit pkgs; };
in
(pkgs.python3.withPackages (ps: [
  trap-dac-utils
])).env
