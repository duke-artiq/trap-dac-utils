import typing
import os
import os.path
import contextlib
import tempfile

__all__ = ['temp_dir']


@contextlib.contextmanager
def temp_dir() -> typing.Generator[str, None, None]:
    """Context manager to temporally change current working
    directory to a unique temp directory.

    Mainly used for testing.
    The temp directory is removed when the context is exited.
    """

    # Remember the original directory
    orig_dir = os.getcwd()

    with tempfile.TemporaryDirectory() as tmp_dir:
        # Change the directory
        os.chdir(tmp_dir)
        try:
            yield tmp_dir
        finally:
            # Return to the original directory
            os.chdir(orig_dir)
