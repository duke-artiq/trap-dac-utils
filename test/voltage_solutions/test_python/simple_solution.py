def solution(mapping_info):

    solution = []
    vals = ['X', '', 'x', 2.3]
    rows = 3
    labels = ['A', 'B', 'C', 'D']
    for _ in range(rows):
        d = {}
        offset = 0
        for i, label in enumerate(labels):
            d[label] = vals[(i+offset) % len(vals)]
            offset += 1
        solution.append(d)

    return solution
