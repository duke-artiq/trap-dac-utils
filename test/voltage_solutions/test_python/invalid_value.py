import random


def solution(mapping_info):

    solution = []
    vals = ['1', '0', 'x']
    rows = 4
    labels = ['A', 'B', 'C']
    for _ in range(rows):
        d = {}
        for label in labels:
            d[label] = random.choice(vals)
        solution.append(d)

    solution[1][labels[0]] = True
    return solution
