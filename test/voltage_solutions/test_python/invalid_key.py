import random


def solution(mapping_info):

    solution = []
    vals = ['1', '0', 'x', '2.3']
    rows = 4
    labels = ['A', 'B', 2.4]
    for _ in range(rows):
        d = {}
        for label in labels:
            d[label] = random.choice(vals)
        solution.append(d)

    return solution
