import random


def solution(mapping_info):

    if not isinstance(mapping_info, dict):
        raise TypeError('Wrong Type')
    solution = []
    vals = ['1', '0', 'x', '2.3']
    rows = 4
    labels = ['A', 'B', 'C']
    for _ in range(rows):
        d = {}
        for label in labels:
            d[label] = random.choice(vals)
        solution.append(d)

    return solution
