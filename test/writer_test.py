import csv
import pathlib
import numpy as np

from trap_dac_utils.writer import BaseWriter
from trap_dac_utils.types import SpecialCharacter

from test.output import temp_dir

_SOLUTIONS_PATH = pathlib.Path("./test/", "voltage_solutions")


def test_write_simple():
    with temp_dir():
        map_file = [{"label": "A", "channel": "0"},
                    {"label": "B", "channel": "1"},
                    {"label": "C", "channel": "2"},
                    {"label": "D", "channel": "3"}]

        solution = [{"A": 1., "B": 2., "C": 3., "D": float("inf")},
                    {"A": 1., "B": SpecialCharacter.X, "C": 3., "D": 4.},
                    {"A": float("-inf"), "B": 2.121, "C": 3., "D": 4.}]

        output = [["A", "B", "C", "D"],
                  ["1.00", "2.00", "3.00", "inf"],
                  ["1.00", "x", "3.00", "4.00"],
                  ["-inf", "2.12", "3.00", "4.00"]]
        writer = BaseWriter(solution_path=pathlib.Path('.'),
                            map_file=map_file,
                            allowed_specials={"x"},
                            precision=2)

        writer.write_solution(solution, "writer_test.csv")

        with open("writer_test.csv") as f:
            reader = csv.reader(f)
            i = 0
            for row, output_line in zip(reader,
                                        output):
                assert row == output_line
                i += 1

            assert i != 0, 'File is empty'


def test_write_simple_np():
    with temp_dir():
        map_file = [{"label": "A", "channel": "0"},
                    {"label": "B", "channel": "1"},
                    {"label": "C", "channel": "2"},
                    {"label": "D", "channel": "3"}]

        solution = [[1, 2, 3, float('inf')],
                    [1, 2, 3, 4],
                    [float('-inf'), 2, 3, 4]]
        solution_np = np.array(solution, dtype=np.float64)
        labels = ["A", "B", "C", "D"]

        output = [["A", "B", "C", "D"],
                  ["1.0", "2.0", "3.0", "inf"],
                  ["1.0", "2.0", "3.0", "4.0"],
                  ["-inf", "2.0", "3.0", "4.0"]]
        writer = BaseWriter(solution_path=pathlib.Path('.'),
                            map_file=map_file,
                            allowed_specials={"x"},
                            precision=1)

        writer.write_solution_np(solution_np, labels, "writer_test.csv")

        with open("writer_test.csv") as f:
            reader = csv.reader(f)
            i = 0
            for row, output_line in zip(reader,
                                        output):
                assert row == output_line
                i += 1

            assert i != 0, 'File is empty'


def test_write_empty():
    with temp_dir():
        map_file = [{"label": "A", "channel": "0"},
                    {"label": "B", "channel": "1"},
                    {"label": "C", "channel": "2"},
                    {"label": "D", "channel": "3"}]

        solution = []

        writer = BaseWriter(solution_path=pathlib.Path('.'),
                            map_file=map_file)

        writer.write_solution(solution, "writer_test.csv")

        with open("writer_test.csv") as f:
            reader = csv.reader(f)
            assert sum(1 for _ in reader) == 0, 'File is not empty'


def test_bad_file_ext():
    with temp_dir():
        map_file = [{"label": "A", "channel": "0"},
                    {"label": "B", "channel": "1"},
                    {"label": "C", "channel": "2"},
                    {"label": "D", "channel": "3"}]

        solution = []

        writer = BaseWriter(solution_path=pathlib.Path('.'),
                            map_file=map_file,
                            allowed_specials={"x"})

        try:
            writer.write_solution(solution, "writer_test.pdf")
        except ValueError as e:
            assert str(e).startswith("File type should be in list ")


def test_write_already_exists():
    map_file = [{"label": "E", "channel": "0"},
                {"label": "F", "channel": "1"},
                {"label": "G", "channel": "2"},
                {"label": "H", "channel": "3"}]

    solution = [{"E": 5., "F": 6., "G": 7., "H": float("inf")}]

    writer = BaseWriter(solution_path=_SOLUTIONS_PATH,
                        map_file=map_file,
                        allowed_specials={"x"})

    try:
        writer.write_solution(solution, "writer_test.csv")
        assert False
    except FileExistsError as e:
        assert str(e) == "File writer_test.csv already exists"


def test_write_overwrite_file():
    with temp_dir():
        with open("writer_test.csv", 'w') as file:
            file.write("Temp content")

        map_file = [{"label": "E", "channel": "0"},
                    {"label": "F", "channel": "1"},
                    {"label": "G", "channel": "2"},
                    {"label": "H", "channel": "3"}]

        solution = [{"E": 4., "F": 5., "G": 6., "H": float("inf")}]

        writer = BaseWriter(solution_path=pathlib.Path('.'),
                            map_file=map_file,
                            allowed_specials={"x"},
                            precision=1)

        output = [["E", "F", "G", "H"],
                  ["4.0", "5.0", "6.0", "inf"]]
        writer.write_solution(solution, "writer_test.csv", overwrite=True)

        with open("writer_test.csv") as f:
            reader = csv.reader(f)
            i = 0
            for row, output_line in zip(reader,
                                        output):
                assert row == output_line
                i += 1

            assert i != 0, 'File is empty'
