import os

__all__ = ['CI_ENABLED']


def _str_to_bool(s: str) -> bool:
    if s in {'y', 'yes', 't', 'true', 'on', '1', '2'}:
        return True
    elif s in {'n', 'no', 'f', 'false', 'off', '0'}:
        return False
    else:
        raise ValueError(f'Cannot convert "{s}" to a bool')


CI_ENABLED: bool = bool(_str_to_bool(os.getenv('GITLAB_CI', '0')))  # Only GitLab CI
"""True if we are running in a CI environment."""
