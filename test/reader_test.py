import csv
import json
import random
import re
import string
import typing
import pathlib
import yaml
from schema import SchemaError
from unittest.mock import patch

from test.output import temp_dir
from test.environment import CI_ENABLED

from trap_dac_utils.types import LABEL_FIELD, HashedTuple, Line
from trap_dac_utils.reader import (BaseReader, SpecialCharacter, SOLUTION_T)
from trap_dac_utils.file_loader import (_load_csv,
                                        _load_json,
                                        _load_py,
                                        _load_yaml)
from trap_dac_utils.schemas import LINEAR_COMBO, SMART_DMA

_NUM_SAMPLES = 1000 if CI_ENABLED else 100

_LINE_CSV_T = typing.List[typing.Union[float, str]]
_SOLUTION_CSV_T = typing.List[_LINE_CSV_T]
_MAP_CSV_T = typing.List[typing.List[str]]

_SOLUTIONS_PATH = pathlib.Path("./test/", "voltage_solutions")
_CONFIG_FILE = pathlib.Path("./meta")
_RNG = random.Random(None)
# default num channels
NUM_CHANNELS = _RNG.randint(1, 100)


class SimpleReader(BaseReader[str]):

    @property
    def voltage_low(self) -> float:
        return -10.0

    @property
    def voltage_high(self) -> float:
        return 10.0

    def process_specials(self, val: SpecialCharacter) -> str:
        return "Test Success"

    def process_solution(self, solution: SOLUTION_T) -> str:
        return "Test Success"


def clear_csv_cache(func):
    def wrapper(*args):
        _load_csv.cache_clear()
        func(*args)
    return wrapper


def clear_py_cache(func):
    def wrapper(*args):
        _load_py.cache_clear()
        func(*args)
    return wrapper


def clear_json_cache(func):
    def wrapper(*args):
        _load_json.cache_clear()
        func(*args)
    return wrapper


def clear_yaml_cache(func):
    def wrapper(*args):
        _load_yaml.cache_clear()
        func(*args)
    return wrapper


@clear_csv_cache
def test_get_solutions() -> None:
    map_path = pathlib.Path("./test", "channel_maps/numeric.csv")
    reader = SimpleReader(_SOLUTIONS_PATH, map_path)
    all_solutions = reader.list_solutions()
    assert "invalid_value.csv" in all_solutions
    assert "value_out_of_range.csv" in all_solutions
    assert "sequential.csv" in all_solutions
    assert "test_python/simple_solution.py" in all_solutions
    assert "test_glob/second_sequential.csv" in all_solutions


@clear_csv_cache
def test_solution_reading_valid() -> None:
    map_path = pathlib.Path("./test/", "channel_maps/numeric.csv")
    reader = SimpleReader(_SOLUTIONS_PATH, map_path)
    solutions_dict_list = reader.read_solution("sequential.csv")
    assert isinstance(solutions_dict_list, SOLUTION_T)
    assert len(solutions_dict_list) == 3
    for i, solutions_dict in enumerate(solutions_dict_list):
        assert len(solutions_dict) == 5
        assert all(x in solutions_dict
                   for x in ['A', 'B', 'C', 'D', 'Z'])
        assert all(isinstance(x, float) or isinstance(x, SpecialCharacter)
                   for x in solutions_dict.values())
        # Test for empty cell handling
        if i == 1:
            assert solutions_dict['A'] == SpecialCharacter('x')


@clear_csv_cache
def test_solution_reading_slice() -> None:
    map_path = pathlib.Path("./test/", "channel_maps/numeric.csv")
    reader = SimpleReader(_SOLUTIONS_PATH, map_path)
    solutions_dict_list = reader.read_solution("sequential.csv", 1, 2)
    assert isinstance(solutions_dict_list, SOLUTION_T)
    assert len(solutions_dict_list) == 2
    for i, solutions_dict in enumerate(solutions_dict_list):
        assert len(solutions_dict) == 5
        assert all(x in solutions_dict
                   for x in ['A', 'B', 'C', 'D', 'Z'])
        assert all(isinstance(x, float) or isinstance(x, SpecialCharacter)
                   for x in solutions_dict.values())
        # Test for empty cell handling
        if i == 0:
            assert solutions_dict['A'] == SpecialCharacter('x')


@clear_csv_cache
def test_duplicate_header() -> None:
    map_path = pathlib.Path("./test/", "channel_maps/numeric.csv")
    reader = SimpleReader(_SOLUTIONS_PATH, map_path)
    try:
        reader.read_solution("duplicate_header.csv")
        # Did not catch value error
        assert False
    except ValueError as e:
        assert str(e) == "All headers in duplicate_header.csv must be unique"


@clear_csv_cache
def test_py_solution_reading_valid() -> None:
    map_path = pathlib.Path("./test/", "channel_maps/numeric.csv")
    reader = SimpleReader(_SOLUTIONS_PATH, map_path)
    solutions_dict_list = reader.read_solution(
        "test_python/simple_solution.py")
    assert isinstance(solutions_dict_list, tuple)
    assert len(solutions_dict_list) == 3
    for i, solutions_dict in enumerate(solutions_dict_list):
        assert len(solutions_dict) == 5
        assert all(x in solutions_dict for x in ['A', 'B', 'C', 'D'])
        assert all(isinstance(solutions_dict[x], (SpecialCharacter, float))
                   for x in solutions_dict.keys())


@clear_py_cache
def test_valid_py_loading() -> None:
    file_path = pathlib.Path(
        './test/', 'voltage_solutions/test_python/simple_solution.py')
    map_file: typing.Sequence[typing.Dict[str, str]] = tuple()
    solution = _load_py(file_path, map_file)
    assert len(solution) == 3
    for d in solution:
        assert len(d) == 4
        assert all(elem in d for elem in ['A', 'B', 'C', 'D'])


@clear_py_cache
def test_invalid_key_py_loading() -> None:
    file_path = pathlib.Path(
        './test/', 'voltage_solutions/test_python/invalid_key.py')
    map_file: typing.Sequence[typing.Dict[str, str]] = tuple()
    try:
        _load_py(file_path, map_file)
        assert False
    except TypeError as e:
        assert str(e) == 'Return signature should be (Sequence[Dict[str,str|float]])'


@clear_py_cache
def test_invalid_value_py_loading() -> None:
    file_path = pathlib.Path(
        './test/', 'voltage_solutions/test_python/invalid_value.py')
    map_file: typing.Sequence[typing.Dict[str, str]] = tuple()
    try:
        _load_py(file_path, map_file)
        assert False
    except TypeError as e:
        assert str(e) == 'Return signature should be (Sequence[Dict[str,str|float]])'


@clear_py_cache
def test_invalid_arg_py_loading() -> None:
    file_path = pathlib.Path(
        './test/', 'voltage_solutions/test_python/invalid_argument.py')
    map_file: typing.Sequence[typing.Dict[str, str]] = tuple()
    try:
        _load_py(file_path, map_file)
        assert False
    except TypeError as e:
        assert str(e) == 'Argument signature should be (Sequence[Dict[str, str]])'


@clear_csv_cache
def test_solution_loading_all_valid() -> None:
    map_path = pathlib.Path("./test/", "channel_maps/numeric.csv")
    reader = SimpleReader(_SOLUTIONS_PATH, map_path)
    all_solutions = reader.read_all_solutions("**/*sequential.csv")
    assert isinstance(all_solutions, dict)
    assert len(all_solutions) == 2
    for file_name, solutions_dict_list in all_solutions.items():
        assert pathlib.Path(str(_SOLUTIONS_PATH) + '/' + file_name).is_file()
        assert len(solutions_dict_list) == 3
        for solution_dict in solutions_dict_list:
            assert len(solution_dict) == 5
            assert all(x in solution_dict
                       for x in ['A', 'B', 'C', 'D', 'Z'])
            assert all(isinstance(x, float) or isinstance(x, SpecialCharacter)
                       for x in solution_dict.values())


@clear_csv_cache
def test_solution_invalid_header() -> None:
    map_path = pathlib.Path("./test/", "channel_maps/numeric.csv")
    reader = SimpleReader(_SOLUTIONS_PATH, map_path)
    try:
        reader.read_solution("invalid_header.csv")
        # Did not catch value error
        assert False
    except ValueError as e:
        assert (str(e) == "Headers in invalid_header.csv must be a "
               "map file label invalid labels: ['E']")


@clear_csv_cache
def test_solution_invalid_value() -> None:
    map_path = pathlib.Path("./test/", "channel_maps/numeric.csv")
    reader = SimpleReader(_SOLUTIONS_PATH, map_path)
    try:
        reader.read_solution("invalid_value.csv")
        # Did not catch value error
        assert False
    except ValueError as e:
        assert str(e) == "Character b is not float or special character"


@clear_csv_cache
def test_solution_disable_special() -> None:
    map_path = pathlib.Path("./test/", "channel_maps/numeric.csv")
    disable_special_reader = SimpleReader(
        _SOLUTIONS_PATH, map_path, frozenset())
    try:
        disable_special_reader.read_solution("sequential.csv")
        # Did not catch value error
        assert False
    except ValueError as e:
        assert str(e) == "Special character x is disabled"


@clear_csv_cache
def test_solution_out_of_range() -> None:
    map_path = pathlib.Path("./test/", "channel_maps/numeric.csv")
    reader = SimpleReader(_SOLUTIONS_PATH, map_path)
    try:
        reader.read_solution("value_out_of_range.csv")
        # Did not catch value error
        assert False
    except ValueError as e:
        assert str(e) == f"Voltage 11 must be in range {reader.voltage_low} to {reader.voltage_high}"


@clear_csv_cache
def test_channel_map_loading_valid() -> None:
    map_path = pathlib.Path("./test/", "channel_maps/numeric.csv")
    reader = SimpleReader(_SOLUTIONS_PATH, map_path)
    map_dict_list = reader.map_file
    assert isinstance(map_dict_list, tuple)
    assert len(map_dict_list) == 5
    for map_dict in map_dict_list:
        assert len(map_dict) == 2
        assert all(x in map_dict.keys() for x in ['label', 'channel'])
        assert all(isinstance(x, str) for x in map_dict.values())


@clear_csv_cache
def test_map_multiple_columns() -> None:
    map_path = pathlib.Path("./test/", "channel_maps/multiple_columns.csv")
    reader = SimpleReader(_SOLUTIONS_PATH, map_path)
    map_dict_list = reader.map_file
    assert isinstance(map_dict_list, tuple)
    assert len(map_dict_list) == 4
    for map_dict in map_dict_list:
        assert len(map_dict) == 3
        assert all(x in map_dict.keys() for x in ['label', 'channel', 'extra'])
        assert all(isinstance(x, str) for x in map_dict.values())


def test_map_missing_label_column() -> None:
    assert_map_error(pathlib.Path("./test/", "channel_maps/missing_label.csv"),
                     "Channel map missing_label.csv must contain a label column")


def test_map_too_few_columns() -> None:
    assert_map_error(pathlib.Path("./test/", "channel_maps/one_column.csv"),
                     "Channel map one_column.csv does not contain enough columns")


def test_map_too_few_rows() -> None:
    assert_map_error(pathlib.Path("./test/", "channel_maps/no_rows.csv"),
                     "Channel map no_rows.csv does not contain any data rows")


def test_map_duplicate_label() -> None:
    assert_map_error(pathlib.Path("./test/", "channel_maps/duplicate_label.csv"),
                     "Channel map duplicate_label.csv labels must be unique")


def test_map_bad_header_start() -> None:
    assert_map_error(pathlib.Path("./test/", "channel_maps/header_starts_with__.csv"),
                     "Channel map header_starts_with__.csv headers must not start with _")


def test_map_invalid_header() -> None:
    assert_map_error(pathlib.Path("./test/",   "channel_maps/invalid_header.csv"),
                     "Channel map invalid_header.csv headers must be python identifiers")


def test_map_invalid_label() -> None:
    assert_map_error(pathlib.Path("./test/", "channel_maps/invalid_label.csv"),
                     "Channel map invalid_label.csv labels must be python identifiers")


def test_map_bad_label_start() -> None:
    assert_map_error(pathlib.Path("./test/",  "channel_maps/label_starts_with__.csv"),
                     "Channel map label_starts_with__.csv labels must not start with _")


@clear_csv_cache
def assert_map_error(map_path, error_str):
    try:
        SimpleReader(_SOLUTIONS_PATH, map_path)
        # Did not catch value error
        assert False
    except ValueError as e:
        assert str(e) == error_str


@clear_csv_cache
def test_read_solution() -> None:
    for _ in range(_NUM_SAMPLES):
        with temp_dir():
            # Write configuration
            global NUM_CHANNELS
            NUM_CHANNELS = _RNG.randint(1, 100)
            solution_file = './voltage_solution.csv'
            map_file = './channel_map.csv'
            input_solution = generate_random_solution()
            expected_channel_map = generate_random_channel_map(
                input_solution[0])
            with open(solution_file, mode='w+') as sf:
                csvWriter = csv.writer(sf, delimiter=',')
                csvWriter.writerows(input_solution)
            with open(map_file, mode='w+') as mf:
                csvWriter = csv.writer(mf, delimiter=',')
                csvWriter.writerows(expected_channel_map)

            reader = SimpleReader(pathlib.Path('.'), pathlib.Path(map_file))
            result_solution = reader.read_solution(
                solution_file)
            for i, d in enumerate(result_solution):
                for k, v in d.items():
                    column = input_solution[0].index(k)
                    if isinstance(v, SpecialCharacter):
                        assert input_solution[i+1][column] == v.value
                    else:
                        assert input_solution[i+1][column] == v
        _load_csv.cache_clear()


def generate_random_solution() -> _SOLUTION_CSV_T:
    solution_size = _RNG.randint(1, 50)
    headers: _LINE_CSV_T = [rand_str()
                            for _ in range(NUM_CHANNELS)]
    lines = [headers]
    special = [str(e.value) for e in SpecialCharacter]
    for i in range(solution_size):
        line: _LINE_CSV_T = []
        for _ in range(NUM_CHANNELS):
            pool: _LINE_CSV_T = [*special, _RNG.uniform(float(-10),
                                                        float(10))]
            v = _RNG.choice(pool)
            line.append(v)
        lines.append(line)
    return lines


def generate_random_channel_map(pins: _LINE_CSV_T) -> _MAP_CSV_T:
    for pin in pins:
        assert isinstance(pin, str)
    headers = ['channels', 'extra', 'last', LABEL_FIELD]
    lines = [headers]
    for i in range(NUM_CHANNELS):
        lines.append([rand_str(), rand_str(), rand_str(), str(pins[i])])
    return lines


def rand_str() -> str:
    return ''.join(_RNG.choice(string.ascii_letters)
                   for _ in range(_RNG.randint(8, 15)))


@clear_csv_cache
def test_hash_solution():
    with temp_dir():
        solution_file = './voltage_solution.csv'
        map_file = './channel_map.csv'
        map_file_text = [['label', 'channel'],
                         ['A', '0'],
                         ['B', '1'],
                         ['C', '2'],
                         ['D', '3']]
        solution_file_text = [['A', 'B', 'C', 'D'],
                              [1., 2., 3., 4.],
                              ['x', 3., 4., 5.],
                              [3., 4., 5., 6.]]
        with open(solution_file, mode='w+') as sf:
            csvWriter = csv.writer(sf, delimiter=',')
            csvWriter.writerows(solution_file_text)
            sf.close()
        with open(map_file, mode='w+') as mf:
            csvWriter = csv.writer(mf, delimiter=',')
            csvWriter.writerows(map_file_text)
            mf.close()

        reader = SimpleReader(pathlib.Path('.'), pathlib.Path(map_file))
        solutions_dict_list = reader.read_solution(solution_file, 0, 1)
        sol_hash = solutions_dict_list.hash

        # Change outside range
        with open(solution_file, 'r+') as f:
            text = f.read()
            text = re.sub('6', '7', text)
            f.seek(0)
            f.write(text)
            f.truncate()
            f.close()

        _load_csv.cache_clear()
        solutions_dict_list2 = reader.read_solution(solution_file, 0, 1)
        sol_hash2 = solutions_dict_list2.hash
        assert sol_hash == sol_hash2

        # Trivial change
        with open(solution_file, 'r+') as f:
            text = f.read()
            text = re.sub('x', 'X', text)
            f.seek(0)
            f.write(text)
            f.close()

        _load_csv.cache_clear()
        solutions_dict_list3 = reader.read_solution(solution_file, 0, 1)
        sol_hash3 = solutions_dict_list3.hash
        assert sol_hash == sol_hash3

        # Change inside range
        with open(solution_file, 'r+') as f:
            text = f.read()
            text = re.sub('1', '2', text)
            f.seek(0)
            f.write(text)
            f.close()

        _load_csv.cache_clear()
        solutions_dict_list4 = reader.read_solution(solution_file, 0, 1)
        sol_hash4 = solutions_dict_list4.hash
        assert sol_hash != sol_hash4


@patch.object(SimpleReader, '_read_channel_map')
@clear_json_cache
def test_load_config_json_valid(_):
    with temp_dir():
        d = {"params": [{"name": "dx",
                         "file": "sequential.csv",
                         "line": 1,
                         "args":
                        {"unit": "V/m",
                         "min": -100.1,
                         "max": 100.1,
                         "ndecimals": 4}},
                        {"name": "dy"}]}
        json_object = json.dumps(d, indent=4)
        fname = f"{_CONFIG_FILE}.json"
        with open(fname, "w+") as file:
            file.write(json_object)
        map_path = pathlib.Path("./test", "channel_maps/numeric.csv")
        reader = SimpleReader(pathlib.Path('.'),
                              map_path)
        cfg = reader.read_config(fname, schema=LINEAR_COMBO)
        assert d == cfg


@patch.object(SimpleReader, '_read_channel_map')
@clear_json_cache
def test_load_config_json_invalid(_):
    with temp_dir():
        d = {"params": [{"name": "dx", "args":
                        {"unit": "V/m",
                         "min": -100.1,
                         "max": 100.1,
                         "ndecimals": 4}},
                        {"args":
                        {"unit": "V/m",
                         "min": -100.1,
                         "max": 100.1,
                         "ndecimals": 4}}]}
        json_object = json.dumps(d, indent=4)
        fname = f"{_CONFIG_FILE}.json"
        with open(fname, "w+") as file:
            file.write(json_object)
        map_path = pathlib.Path("./test", "channel_maps/numeric.csv")
        reader = SimpleReader(pathlib.Path('.'),
                              map_path)
        try:
            reader.read_config(fname, schema=LINEAR_COMBO)
            # Did not throw a schema error
            assert False
        except SchemaError:
            # Did throw a schema error
            pass


@patch.object(SimpleReader, '_read_channel_map')
@clear_yaml_cache
def test_load_config_yaml_valid(_):
    with temp_dir():
        d = {"params": [{"name": "dx",
                         "file": "sequential.csv",
                         "line": 1,
                         "args":
                        {"unit": "V/m",
                         "min": -100.1,
                         "max": 100.1,
                         "ndecimals": 4}},
                        {"name": "dy"}]}
        fname = f"{_CONFIG_FILE}.yaml"
        with open(fname, "w+") as file:
            yaml.dump(d, file)
        map_path = pathlib.Path("./test", "channel_maps/numeric.csv")
        reader = SimpleReader(pathlib.Path('.'),
                              map_path)

        cfg = reader.read_config(fname, schema=LINEAR_COMBO)
        assert d == cfg


@patch.object(SimpleReader, '_read_channel_map')
@clear_yaml_cache
def test_load_config_yaml_invalid(_):
    with temp_dir():
        d = {"params": [{"name": "dx", "args":
                        {"unit": "V/m",
                         "min": -100.1,
                         "max": 100.1,
                         "ndecimals": 4}},
                        {"args":
                        {"unit": "V/m",
                         "min": -100.1,
                         "max": 100.1,
                         "ndecimals": 4}}]}
        fname = f"{_CONFIG_FILE}.yaml"
        with open(fname, "w+") as file:
            yaml.dump(d, file)
        map_path = pathlib.Path("./test", "channel_maps/numeric.csv")

        reader = SimpleReader(pathlib.Path('.'),
                              map_path)
        try:
            reader.read_config(fname, schema=LINEAR_COMBO)
            # Did not throw a schema error
            assert False
        except SchemaError:
            # Did throw a schema error
            pass


@patch.object(SimpleReader, '_read_channel_map')
@clear_yaml_cache
def test_smart_dma_schema_valid(_):
    with temp_dir():
        d = {"params": [{"name": "s1", "file": "solutions.csv",
                         "start": 1, "end": 3, "reverse": False,
                         "multiplier": 2.3, "line_delay": 10.0},
                        {"name": "s2", "line_delay": 20.0}]}
        fname = f"{_CONFIG_FILE}.yaml"
        with open(fname, "w+") as file:
            yaml.dump(d, file)
        map_path = pathlib.Path("./test", "channel_maps/numeric.csv")
        reader = SimpleReader(pathlib.Path('.'),
                              map_path)

        cfg = reader.read_config(fname, schema=SMART_DMA)
        assert d == cfg


@patch.object(SimpleReader, '_read_channel_map')
@clear_yaml_cache
def test_smart_dma_schema_invalid(_):
    with temp_dir():
        d = {"params": [{"name": "s1", "file": "solutions.csv",
                         "start": 1, "end": 3, "reverse": False,
                         "multiplier": 2.3},
                        {"name": "s2", "file": "solutions.csv", "line_delay": 20.0}]}
        fname = f"{_CONFIG_FILE}.yaml"
        with open(fname, "w+") as file:
            yaml.dump(d, file)
        map_path = pathlib.Path("./test", "channel_maps/numeric.csv")

        reader = SimpleReader(pathlib.Path('.'),
                              map_path)
        try:
            reader.read_config(fname, schema=SMART_DMA)
            # Did not throw a schema error
            assert False
        except SchemaError:
            # Did throw a schema error
            pass


def test_hashed_tuple_add():
    a = HashedTuple((Line({"A": 1., "B": 1.}), Line({"C": 1., "D": SpecialCharacter.X}),))
    b = HashedTuple((Line({"A": 2., "B": 3.}), Line({"C": 4., "D": SpecialCharacter.X}),))
    output = ({"A": 3., "B": 4.}, {"C": 5., "D": SpecialCharacter.X})
    c = a + b
    assert isinstance(c, HashedTuple)
    assert len(c) == 2
    assert c[0] == output[0] and c[1] == output[1]


def test_hashed_tuple_mul():
    a = HashedTuple((Line({"A": 1., "B": 0.}), Line({"C": 0., "D": SpecialCharacter.X}),))
    b = HashedTuple((Line({"A": 2., "B": 3.}), Line({"C": 4., "D": SpecialCharacter.X}),))
    output = ({"A": 2., "B": 0.}, {"C": 0., "D": SpecialCharacter.X})
    c = a * b
    assert isinstance(c, HashedTuple)
    assert len(c) == 2
    assert c[0] == output[0] and c[1] == output[1]


def test_hashed_tuple_add_invalid():
    a = HashedTuple((Line({"A": 1., "B": 1.}), Line({"C": 1., "D": SpecialCharacter.X}),))
    b = HashedTuple((Line({"A": 2., "B": 3.}), Line({"C": 4., "D": 5.}),))
    try:
        a + b
    except ValueError as e:
        assert str(e) == ("operation + resulted in a banned float value self Line({'C': 1.0, 'D': "
                          "<SpecialCharacter.X: 'x'>}) other Line({'C': 4.0, 'D': 5.0}) "
                          "result Line({'C': 5.0, 'D': nan})")
    else:
        assert False


def test_hashed_tuple_mul_invalid():
    a = HashedTuple((Line({"A": 1., "B": 0.}), Line({"C": 0., "D": SpecialCharacter.X}),))
    b = HashedTuple((Line({"A": 2., "B": 3.}), Line({"C": 4., "D": 5.}),))
    try:
        a * b
    except ValueError as e:
        assert str(e) == ("operation * resulted in a banned float value self Line({'C': 0.0, 'D': "
                          "<SpecialCharacter.X: 'x'>}) other Line({'C': 4.0, 'D': 5.0}) "
                          "result Line({'C': 0.0, 'D': nan})")
    else:
        assert False


def test_special_char_add_invalid():
    x = SpecialCharacter.X
    try:
        x + 'a'
    except TypeError as e:
        assert str(e) == "unsupported operand type(s) for +: '<enum 'SpecialCharacter'>' and '<class 'str'>'"
    else:
        assert False


def test_special_char_mul_invalid():
    x = SpecialCharacter.X
    try:
        x * 'a'
    except TypeError as e:
        assert str(e) == "unsupported operand type(s) for *: '<enum 'SpecialCharacter'>' and '<class 'str'>'"
    else:
        assert False


def test_banned_special():
    map_path = pathlib.Path("./test", "channel_maps/numeric.csv")
    reader = SimpleReader(pathlib.Path('.'),
                          map_path)

    try:
        reader._process_solution_value('nan')
    except ValueError as e:
        assert str(e) == "Value cannot be nan"
    else:
        assert False


def test_verify_solution():
    map_path = pathlib.Path("./test", "channel_maps/numeric.csv")
    reader = SimpleReader(pathlib.Path('.'),
                          map_path)

    a = HashedTuple(({"A": 2., "B": 3.}, {"C": 20., "D": SpecialCharacter.X},))
    assert not reader.verify_solution(a)
