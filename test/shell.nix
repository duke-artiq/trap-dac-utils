{ pkgs ? import <nixpkgs> { }
}:

let
  trap-dac-utils = pkgs.python3Packages.callPackage ../derivation.nix { };
in
pkgs.mkShell {
  buildInputs = [
    (pkgs.python3.withPackages (ps: [
      # Packages required for testing
      ps.pytest
      ps.coverage
      ps.mypy
      ps.flake8
      ps.numpy
    ] ++ trap-dac-utils.propagatedBuildInputs))
  ];
}
