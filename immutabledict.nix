{ buildPythonPackage
, fetchFromGitHub
, poetry-core
, pytestCheckHook
}:

# https://github.com/NixOS/nixpkgs/blob/nixos-22.05/pkgs/development/python-modules/immutabledict/default.nix
buildPythonPackage rec {
  pname = "immutabledict";
  version = "2.2.1";
  src = fetchFromGitHub {
    owner = "corenting";
    repo = "immutabledict";
    rev = "v${version}";
    sha256 = "sha256-z04xxoCw0eBtkt++y/1yUsAPaLlAGUtWBdRBM74ul1c=";
  };
  format = "pyproject";

  nativeBuildInputs = [
    poetry-core
  ];

  checkInputs = [
    pytestCheckHook
  ];

  pythonImportsCheck = [
    "immutabledict"
  ];
}
