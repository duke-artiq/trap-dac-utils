# Usage guidelines

This file contains usage guidelines for this library that fall outside of the specification.

## Solution files

To use the solution files properly the experiment must start with a fully defined line such that all hardware output
channels will be set. This will eliminate any unwanted voltage skews and help to define the `"x"` values throughout the
rest of the experiment.